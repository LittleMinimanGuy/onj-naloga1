str = '''
Ljubljana 	258.873 			med 1220 in 1243
Maribor 	151.349 			13. stoletje
Celje 	37.834 			11. april 1451
Kranj 	35.587 			1256
Koper 	25.306[7] 			pred 1230
Velenje 	24.923[14] 			20. september 1959
Ptuj 	23.957 	mlajša kamena doba 	69 	1376
Novo mesto 	22.415 			7. april 1365
Trbovlje 	17.485 			1952
Nova Gorica 	13.491 			1948 	1455 italijanska Gorica
Jesenice 	13.429 			1929
Murska Sobota 	12.437 			1366 	prva omemba kot mesto
Škofja Loka 	12.289 			1274
Kamnik 	12.197 	1061 		pred 1229
Domžale 	11.582 			1951
Izola 	10.381 			1253
Kočevje 	9.027 			19. april 1471
Postojna 	8.548 			3. maj 1909
Ravne na Koroškem 	8.507 			1952
Slovenj Gradec 	7.712 			22. maj 1267
Logatec 	7.616 			22. december 2005
Vrhnika 	7.520 			1955
Krško 	6.994 	895[8] 		5. marec 1477
Zagorje ob Savi 	6.893 	1296
Brežice 	6.856 	1241 	začetek 14. stoletja[5] 	1322[6]
Slovenska Bistrica 	6.591 			1313
Litija 	6.420 			1952
Ajdovščina 	6.373 		1507[4] 	1955
Grosuplje 	6.050 			22. december 2005
Radovljica 	5.937 			1473
Idrija 	5.878 			začetek 18. stoletja
Hrastnik 	5.865
Črnomelj 	5.854 			1407
Mengeš 	5.557 			22. december 2005
Sežana 	5.332 	1060[10] 		1952
Bled 	5.252 	1004 		1960
Sevnica 	4.933 		1322[6] 	1959
Žalec 	4.919 			1964
Ilirska Bistrica 	4.869 			1933
Slovenske Konjice 	4.866 	1146[11] 	1251[12] 	1964
Rogaška Slatina 	4.801 			22. december 2005
Šentjur 	4.723 			22. december 2005
Medvode 	4.655 			22. december 2005
Laško 	4.619 			1927
Ruše 	4.571 			22. december 2005
Prevalje 	4.504 			22. december 2005
Piran 	4.143
Cerknica 	3.981 			22. december 2005
Tržič 	3.920 			1926 [13]
Šempeter pri Gorici 	3.865 			22. december 2005
Tolmin 	3.737 			1952
Žiri 	3.593 			22. december 2005
Gornja Radgona 	3.529 			1952 	slovenski del, sicer pa 1299
Mežica 	3.487 			22. december 2005
Ribnica 	3.480[9] 	ok. 855 	1315 	1480
Dravograd 	3.414 			22. december 2005
Ljutomer 	3.413 			1927
Lendava 	3.395 			1867
Metlika 	3.225 			pred 1335
Železniki 	3.156 			22. december 2005
Trebnje 	3.150 			22. december 2005
Lenart v Slovenskih goricah 	3.006 	1196 	pred 1332 	29. november 1989
Šoštanj 	2.793 			1911
Radeče 	2.296 			25. avgust 1925
Ormož 	2.151 			1331
Bovec 	1.612 			1951
Vipava 	1.566 			1478
Turnišče 	1.555 			1548
Kobarid 	1.238 			1895
Gornji Grad 	1.017 			1929
Višnja Gora 	813 			1478
Kostanjevica na Krki 	751 			okoli 1215 	kovanec z napisom CIVITAS CANDESTRO; prva omemba mesta 1252
Vipavski Križ
'''
print()
for line in str.split('\n'):
    words = line.strip().split(" \t")
    if len(words) > 1:
        print("\"{}\": {},".format(words[0].strip(), words[1].replace(".", "")))
