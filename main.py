from bs4 import BeautifulSoup
import requests
import csv
import time
import re
from multiprocessing.dummy import Pool as ThreadPool
import nltk
from geotext import GeoText
import pickle
from collections import defaultdict
from matplotlib import pyplot as plt
import shapefile

# https://sl.wikipedia.org/wiki/Seznam_mest_v_Sloveniji
# https://github.com/GeospatialPython/pyshp
# https://geotext.readthedocs.io/en/latest/readme.html#usage
# https://egp.gu.gov.si/egp/

# todo poglej še časovno komponenot

pop_na_kraj = {
    "Ljubljana": 258873,
    "Maribor": 151349,
    "Celje": 37834,
    "Kranj": 35587,
    "Koper": 25306,
    "Velenje": 24923,
    "Ptuj": 23957,
    "Novo mesto": 22415,
    "Trbovlje": 17485,
    "Nova Gorica": 13491,
    "Jesenice": 13429,
    "Murska Sobota": 12437,
    "Škofja Loka": 12289,
    "Kamnik": 12197,
    "Domžale": 11582,
    "Izola": 10381,
    "Kočevje": 9027,
    "Postojna": 8548,
    "Ravne na Koroškem": 8507,
    "Slovenj Gradec": 7712,
    "Logatec": 7616,
    "Vrhnika": 7520,
    "Krško": 6994,
    "Zagorje ob Savi": 6893,
    "Brežice": 6856,
    "Slovenska Bistrica": 6591,
    "Litija": 6420,
    "Ajdovščina": 6373,
    "Grosuplje": 6050,
    "Radovljica": 5937,
    "Idrija": 5878,
    "Hrastnik": 5865,
    "Črnomelj": 5854,
    "Mengeš": 5557,
    "Sežana": 5332,
    "Bled": 5252,
    "Sevnica": 4933,
    "Žalec": 4919,
    "Ilirska Bistrica": 4869,
    "Slovenske Konjice": 4866,
    "Rogaška Slatina": 4801,
    "Šentjur": 4723,
    "Medvode": 4655,
    "Laško": 4619,
    "Ruše": 4571,
    "Prevalje": 4504,
    "Piran": 4143,
    "Cerknica": 3981,
    "Tržič": 3920,
    "Šempeter pri Gorici": 3865,
    "Tolmin": 3737,
    "Žiri": 3593,
    "Gornja Radgona": 3529,
    "Mežica": 3487,
    "Ribnica": 3480,
    "Dravograd": 3414,
    "Ljutomer": 3413,
    "Lendava": 3395,
    "Metlika": 3225,
    "Železniki": 3156,
    "Trebnje": 3150,
    "Lenart v Slovenskih goricah": 3006,
    "Šoštanj": 2793,
    "Radeče": 2296,
    "Ormož": 2151,
    "Bovec": 1612,
    "Vipava": 1566,
    "Turnišče": 1555,
    "Kobarid": 1238,
    "Gornji Grad": 1017,
    "Višnja Gora": 813,
    "Kostanjevica na Krki": 751,
}


class MMCgrabber:
    _url = "http://www.rtvslo.si/lokalne-novice/arhiv/"
    _startDate = "2008-10-25"
    _articles = list()

    def _joinkArticle(self, article_url):
        with requests.Session() as s:
            response = s.get(self._url + article_url)
        print(response.url, response)
        page = BeautifulSoup(response.text, 'html.parser')
        try:
            title = page.find("article").find("div", {"class": "header"}).find("h1").get_text()
            text = [p.get_text() for p in page.find("article").find("div", {"class": "header"}).find_next_siblings("p")]

        except AttributeError:
            try:
                title = page.find("div", {"id": "module-content"}).find("div", {"class": "title"}).find("h1").get_text()
                text = [p.get_text() for p in page.find("div", {"id": "newsbodyx"}).find("div").find_next_siblings("p")]
            except AttributeError:
                try:
                    title = page.find("div", {"class": "title"}).get_text()
                    text = [p.get_text() for p in page.find("div", {"id": "newsbody"}).find("h1").find_next_siblings("p")]
                except AttributeError:
                    print("Error while trying to read this adress ^")
                    return

        self._articles.append(" ".join((title, " ".join(text))))

    def _getAllArticleUrlsForPage(self, page):
        params = {
            "date_from": self._startDate,
            "page": page
        }
        with requests.Session() as s:
            response = s.get(self._url, params=params)
        # print(response)
        page = response.text
        soup = BeautifulSoup(page, 'html.parser')
        articles_urls = [a["href"] for a in soup.find(id="contents").find_next(id="sectionlist").find_next(id="sectionlist").find_all("a", {"class": "title"})]
        for articles_url in articles_urls:
            self._joinkArticle(articles_url)

    def writeToCsv(self):
        ts = time.time()
        with open('archiveJoink' + str(int(ts)) + '.csv', 'w', encoding='UTF-8') as f:  # Just use 'w' mode in 3.x
            for article in self._articles:
                f.write(re.sub('\s+', ' ', article).strip() + "\n")

    def __init__(self):
        # hand checked number of pages to be 325
        pages = (i for i in range(326))
        pool = ThreadPool(32)
        pool.map(self._getAllArticleUrlsForPage, pages)
        pool.close()
        pool.join()
        self.writeToCsv()


###### CONVERT CSV TO PICKLE ##########
def articlesToPickle():
    with open("archiveJoink1540545473.csv", encoding='UTF-8') as f:
        articles = f.readlines()

    articles = [str(line) for line in filter(lambda article: len(article) > 1, articles)]

    with open("articles.obj", 'wb') as f:
        pickle.dump(articles, f, protocol=pickle.HIGHEST_PROTOCOL)


def simpleCompare():
    """
    SIMPLE COMPARISON OF CITY SIZE AND NUMBER OF ARTICLES WITH GEOTEXT

    NUM ARTICLES: 6417
    NUM FOUND ARTICLES: 1696
    % OF PLACES FOUND: 0.2642979585476079

    no data for other cities

    shows plot of articles per capita
    """
    top_10 = {
        "Ljubljana": 258873,
        "Maribor": 151349,
        "Celje": 37834,
        "Kranj": 35587,
        "Koper": 25306,
        "Velenje": 24923,
        "Ptuj": 23957,
        "Novo mesto": 22415,
        "Trbovlje": 17485,
        "Nova Gorica": 13491
    }
    with open("articles.obj", 'rb') as f:
        articles = pickle.load(f)
    cities_mentions = defaultdict(int)
    places = [GeoText(article, 'SI') for article in articles]
    found_places = list(map(lambda place: place.cities, filter(lambda place: len(place.cities) > 0, places)))
    # print(found_places)
    print("NUM PLACES:", len(places))
    print("NUM FOUND PLACES:", len(found_places))
    print("% OF PLACES FOUND:", len(found_places) / len(places))
    for place in places:
        city = set(place.cities)
        if len(city) == 1:
            cities_mentions[city.pop()] += 1
    print(cities_mentions)
    x = list()
    y = list()
    for city in cities_mentions:
        x.append(city)
        y.append(cities_mentions[city] / top_10[city])
    plt.bar(x, y)
    plt.show()


def pickleNames():
    sf = shapefile.Reader("REZI/REZI_D48")
    print(sf.fields)
    lastna_imena = set()
    for record in sf.records():
        if record[19] == 5:
            if type(record[4]) is bytes:
                ime = record[4].decode(encoding='windows-1252')
            else:
                ime = record[4]
            lastna_imena.add(ime)

    with open("lastna_imena.obj", 'wb') as f:
        pickle.dump(lastna_imena, f, protocol=pickle.HIGHEST_PROTOCOL)


# nltk.download('punkt')
# articlesToPickle()
with open("articles.obj", 'rb') as f:
    articles = pickle.load(f)

print(articles[0])

with open("lastna_imena.obj", 'rb') as f:
    lastna_imena = pickle.load(f)

identified_articles = defaultdict(int)

# slovene.pickle
print(lastna_imena)
# articles = articles[:10]
slovene_tokinizer = nltk.data.load('tokenizers/punkt/slovene.pickle')
word_tokenizer = nltk.tokenize.WordPunctTokenizer()
# print(slovene_tokinizer.tokenize(articles[0]))

num_found = 0
num_bad_found = 0
num_articles = len(articles)
for article in articles:
    found = False
    foundBad = False
    for sentence in slovene_tokinizer.tokenize(article):
        for word in word_tokenizer.tokenize(sentence):
            if word in pop_na_kraj.keys():
                identified_articles[word] += 1
                found = True
            elif word in lastna_imena:
                foundBad = True
    if found:
        num_found += 1
    if foundBad:
        num_bad_found += 1

"""
NUM ARTICLES: 6417
NUM FOUND ARTICLES: 3223
NUM FOUND BAD ARTICLES: 6069
% OF PLACES FOUND: 0.5022596228767336
% OF BAD PLACES FOUND: 0.9457690509583918
"""

print("NUM ARTICLES:", num_articles)
print("NUM FOUND ARTICLES:", num_found)
print("NUM FOUND BAD ARTICLES:", num_bad_found)
print("% OF PLACES FOUND:", num_found / num_articles)
print("% OF BAD PLACES FOUND:", num_bad_found / num_articles)

# identified_articles = dict((k,v) for k,v in identified_articles.items() if k in pop_na_kraj)
# identified_articles = [(key, val) for key, val in identified_articles.items()]
# identified_articles = sorted(identified_articles, key=lambda el: el[1], reverse=True)
# x = [el[0] for el in identified_articles[:20]]
# y = [el[1] for el in identified_articles[:20]]
# plt.bar(x, y)


# plt.show()

data = list()
for city in identified_articles:
    data.append((city, identified_articles[city] / pop_na_kraj[city]))

data = sorted(data, key=lambda el: el[1], reverse=True)[:20]
x = [el[0] for el in data[:20]]
y = [el[1] for el in data[:20]]
plt.bar(x, y)
plt.show()

# print(lastna_imena)
